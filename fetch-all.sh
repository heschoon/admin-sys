#!/bin/bash

# git backup

OLD_DIR=$(pwd)
if [ ! -d "$GIT_BACKUP_HOME" ]
then
	cd $OLD_DIR
	echo "No GIT_BACKUP_HOME set, exiting"
	exit 1
fi

if [ -d "$GIT_REPOSITORIES" ]
then
	cd $GIT_REPOSITORIES
else
	cd $OLD_DIR
	echo "No GIT_REPOSITORIES set, exiting"
	exit 1
fi

echo "###########################################"
echo " Backuping gitolite@localhost repositories"
for project in $(ls)
do
	if [ -d $project ]
	then
		cp -r $project $GIT_BACKUP_HOME/$project
	else
		cp $project $GIT_BACKUP_HOME/$project
	fi
done
echo "###########################################"


cd $OLD_DIR