#!/bin/bash

# add user for backups

echo "USERADD"
useradd -g git_group -m -s /bin/bash backuper
echo "PASSWORD"
passwd backuper
echo "CD ~/.SSH"
su - backuper -c "cd ~/.ssh"
echo "SSH-KEYGEN"
su - backuper -c "ssh-keygen -f ~/.ssh/id_rsa"
echo "SCP"
su - backuper -c "scp ~/.ssh/id_rsa.pub gitolite@localhost:backuper.pub"
echo "MKDIR BACKUP AND BIN"
mkdir ~backuper/git-backups
chown backuper ~backuper/git-backups
mkdir ~backuper/bin
chown backuper ~backuper/bin
echo "CP SCRIPTS"
cp backup-git.sh ~backuper/bin/backup-git.sh
chown backuper ~backuper/bin/backup-git.sh
cp fetch-all.sh ~backuper/bin/fetch-all.sh
chown backuper ~backuper/bin/fetch-all.sh
crontab -u backuper -l > crontab_init
if ! grep -q "30 5 * * * ~backuper/bin/backup-git.sh > /tmp/git-backup-cron.log" crontab_init
then
	echo "30 5 * * * ~backuper/bin/backup-git.sh > /tmp/git-backup-cron.log" > crontab_init
	crontab -u backuper crontab_init
fi
rm crontab_init
