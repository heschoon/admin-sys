#!/bin/bash

# installation git/gitolite
if [ $# = 1 ]; then
	user=$1
	echo "HOME"
	cd $HOME
	echo "UPDATE"
	apt-get update
	echo "GIT"
	apt-get install git-core
	echo "OPENSSH"
	apt-get install openssh-server
	echo "USERADD"
	groupadd git_group
	useradd -g git_group -m -s /bin/bash gitolite
	echo "DEFINISSEZ UN MOT DE PASSE POUR LE SERVEUR GITOLITE:"
	passwd gitolite
	echo "ATTENTION, LAISSEZ LA PASSPHRASE VIDE!"
	ssh-keygen -f ~/.ssh/id_rsa
	echo "SCP : ENTREZ LE MOT DE PASSE DU SERVEUR GITOLITE:"
	scp ~/.ssh/id_rsa.pub gitolite@localhost:$user.pub
	echo "CP"
	cp /home/gitolite/$user.pub /tmp/$user.pub
	echo "CLONE 1"
	git clone git://github.com/sitaramc/gitolite.git
	echo "CD GITOLITE"
	cd gitolite
	echo "MKDIR"
	mkdir -p /usr/local/share/gitolite/conf /usr/local/share/gitolite/hooks
	echo "GL-SYSTEM-INSTALL"
	src/gl-system-install /usr/local/bin /usr/local/share/gitolite/conf /usr/local/share/gitolite/hooks
	echo "GL-SETUP"
	su - gitolite -c "gl-setup /tmp/$user.pub"
	if [ -d /home/gitolite/gitolite-admin ]; then
		echo "GITOLITE-ADMIN EXISTS"
	else
		echo "CLONE 2"
		su - gitolite -c "git clone ssh://gitolite@localhost/home/gitolite/repositories/gitolite-admin.git"
	fi
	su - gitolite -c "chmod g+rwx /home/gitolite"
	su - gitolite -c "chmod g+rwx /home/gitolite/repositories"
else
	echo "Précisez votre nom d'utilisateur en paramètre"
fi	